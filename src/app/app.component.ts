import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
	public odds:Array<number> = [];
	// public evens:Array<number> = [];
	public evens:number[] = []; // demos another way to do array declaration

	// addNumber(data:{count:number}) {
	// 	if (data.count%2==0) { 
	// 		// even
	// 		this.evens.push(data.count);
	// 	} else {
	// 		// odd
	// 		this.odds.push(data.count);
	// 	}
	// }
	addNumber(count) {
		if (count%2==0) { 
			// even
			this.evens.push(count);
		} else {
			// odd
			this.odds.push(count);
		}
	}
}
