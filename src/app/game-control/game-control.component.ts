import { Component, OnInit,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {
	private interval:NodeJS.Timer;
	private isDisabled:boolean = false;
	// @Output('onGameStart') onGameStart = new EventEmitter<{count:number}>();
	@Output('onGameStart') onGameStart = new EventEmitter<number>();
	constructor() { }

	ngOnInit() {
	}

	startGame() {
		let that:any = this;
		let counter:number = 0;
		this.isDisabled = true;
		this.interval = setInterval(function(){ 
			console.log("game on:", counter, " ", typeof counter);
			// that.onGameStart.emit({count:counter});
			that.onGameStart.emit(counter);
			counter++;
		}, 1000);
	}
	stopGame() {
		this.isDisabled = false;
		clearInterval(this.interval);
	}
}
